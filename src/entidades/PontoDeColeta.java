package entidades;

public class PontoDeColeta {
	
	private String diaDeRetirada;
	private int saldoGerado;
	private Endereco endereco;
	
	public PontoDeColeta(String diaDeRetirada, Endereco endereco) {
		this.diaDeRetirada = diaDeRetirada;
		this.endereco = endereco;
	}
	
	public Endereco getEndereco() {
		return this.endereco;
	}
	public int getSaldoGerado() {
		return saldoGerado;
	}
	public void setSaldoGerado(int saldoGerado) {
		this.saldoGerado = saldoGerado;
	}
	public String getDiaDeRetirada() {
		return diaDeRetirada;
	}
	
	
	

}
