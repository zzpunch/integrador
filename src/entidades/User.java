package entidades;

public abstract class User  {
	
	protected String emailLogin;
	protected String senha;
	protected String nome;
	
	public String getEmailLogin() {
		return emailLogin;
	}
	public void setEmailLogin(String emailLogin) {
		this.emailLogin = emailLogin;
	}
	public String getSenha() {
		return senha;
	}
	public void setSenha(String senha) {
		this.senha = senha;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	
	
	
}
