package entidades;

import java.util.List;

public class Client extends User{
	
	private String telefone;
	private int idade;
	private double saldo;
	private int pontosRank;
	private List<Transacao> transacao; 
	

	public Client(String nome, String emailLogin, String Senha, String telefone, int idade) {
		super();
		this.telefone = telefone;
		this.idade = idade;
	}
	
	public List<Transacao> getTransacao() {
		return transacao;
	}
	
	public void setTransacao(List<Transacao> transacao) {
		this.transacao = transacao;
	}

	public String getNome() {
		return super.nome;
	}

	public String getTelefone() {
		return telefone;
	}

	public int getIdade() {
		return idade;
	}

	public int getPontosRank() {
		return pontosRank;
	}

	public double getSaldo() {
		return saldo;
	}

	public void atualizaPontosRank(int pontos) {
		this.pontosRank += pontos;
	}

	public void atualizaSaldo(double valor) {
		this.saldo += valor;
	}

	

	
	
	
	

}
