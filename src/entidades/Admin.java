package entidades;

public class Admin extends User{

	protected EmpresaParceira empresa;
		
	public Admin(String nome, String email, String senha, EmpresaParceira empresa) {
		super();
		this.empresa = empresa;
	}
	
	public String getNome() {
		return super.nome;
	}
	public String getEmail() {
		return super.emailLogin;
	}
	public String getSenha() {
		return super.senha;
	}
	public EmpresaParceira getEmpresa() {
		return empresa;
	}
	

}
